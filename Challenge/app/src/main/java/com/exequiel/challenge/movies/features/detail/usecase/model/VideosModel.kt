package com.exequiel.challenge.movies.features.detail.usecase.model

import com.exequiel.challenge.movies.core.BaseListResponse
import com.exequiel.challenge.movies.features.detail.datasource.entity.VideoApiResponse
import com.google.gson.annotations.SerializedName

data class VideoListModel(
    override var results: List<VideoModel>
) : BaseListResponse<VideoModel>

data class VideoModel(
    var id: String,
    var key: String,
    var name: String,
)