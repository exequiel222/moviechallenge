package com.exequiel.challenge.movies.features.list.datasource.repository

import com.exequiel.challenge.movies.features.list.datasource.service.IMoviesListService
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesModel
import com.exequiel.challenge.movies.features.list.usecase.toUseCaseModel

class MoviesListRepository(private val moviesListService: IMoviesListService) {

    @Throws(Exception::class)
    suspend fun callGetPopularList(page: Int): MoviesModel {
        return moviesListService.fetchPopularList(page).toUseCaseModel()
    }
}