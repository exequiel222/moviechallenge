package com.exequiel.challenge.movies.features.list.datasource.entity

import com.google.gson.annotations.SerializedName

data class GenreApiResponse(
    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,
)