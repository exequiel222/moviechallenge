package com.exequiel.challenge.movies.features.list.navigation

import android.view.View
import androidx.navigation.Navigation
import com.exequiel.challenge.movies.features.list.view.MoviesListFragmentDirections

class MoviesListNavigation {
    fun goToDetailMovie(view: View, id: Int){
        val action = MoviesListFragmentDirections.actionMoviesListEntryToMoviesDetailEntry(id)
        Navigation.findNavController(view).navigate(action)
    }
}