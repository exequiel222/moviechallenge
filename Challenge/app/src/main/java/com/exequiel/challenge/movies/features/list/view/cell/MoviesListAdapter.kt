package com.exequiel.challenge.movies.features.list.view.cell

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.exequiel.challenge.databinding.MovieListItemGridBinding
import com.exequiel.challenge.databinding.MoviesListItemListBinding
import com.exequiel.challenge.movies.core.InfiniteContentScrollListener
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class MoviesListAdapter internal constructor(
    private val onItemClickListener: IOnItemClickViewHolder,
    private val infiniteContentScrollListener: InfiniteContentScrollListener
) : ListAdapter<(MoviesItemModel), MoviesListViewHolder>(MovieDiffCallback()) {

    private var isMovieItemGrid: Boolean = false

    override fun submitList(list: List<MoviesItemModel>?) {
        val newList: MutableList<MoviesItemModel> = arrayListOf()
        if (list != null) newList.addAll(list)
        super.submitList(newList)
        infiniteContentScrollListener.itemsLoaded()
    }

    override fun onBindViewHolder(holder: MoviesListViewHolder, position: Int) {
        holder.bindingDataInHolder(getItem(position))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        if (recyclerView.layoutManager is GridLayoutManager) isMovieItemGrid = true
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewBinding = if (isMovieItemGrid) {
            MovieListItemGridBinding.inflate(layoutInflater, parent, false)
        } else {
            MoviesListItemListBinding.inflate(layoutInflater, parent, false)
        }
        return MoviesListViewHolder(binding, onItemClickListener)
    }

    private class MovieDiffCallback : DiffUtil.ItemCallback<MoviesItemModel>() {
        override fun areItemsTheSame(oldItem: MoviesItemModel, newItem: MoviesItemModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: MoviesItemModel, newItem: MoviesItemModel): Boolean {
            return oldItem.id == newItem.id
        }
    }
}
