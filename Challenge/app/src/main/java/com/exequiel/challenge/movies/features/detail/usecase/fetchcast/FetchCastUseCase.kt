package com.exequiel.challenge.movies.features.detail.usecase.fetchcast

import com.exequiel.challenge.movies.core.usecase.BaseUseCase
import com.exequiel.challenge.movies.features.detail.datasource.repository.MovieDetailRepository
import com.exequiel.challenge.movies.features.detail.usecase.model.CreditsListModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoListModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class FetchCastUseCase(
    private val movieDetailRepository: MovieDetailRepository
) : BaseUseCase<Int, CreditsListModel>() {

    override suspend fun run(params: Int): CreditsListModel {
        return movieDetailRepository.callGetCreditsMovie(params)
    }
}