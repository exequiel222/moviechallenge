package com.exequiel.challenge.movies.features.detail.view

import android.view.View
import androidx.navigation.fragment.navArgs
import com.exequiel.challenge.R
import com.exequiel.challenge.databinding.MovieDetailFragmenBinding
import com.exequiel.challenge.movies.core.datasource.TheMovieDatabaseAPI
import com.exequiel.challenge.movies.core.toGone
import com.exequiel.challenge.movies.core.toVisible
import com.exequiel.challenge.movies.core.view.BaseFragment
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.core.viewmodel.Event
import com.exequiel.challenge.movies.core.viewmodel.observe
import com.exequiel.challenge.movies.features.detail.navigation.MovieDetailNavigation
import com.exequiel.challenge.movies.features.detail.usecase.model.CastModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoModel
import com.exequiel.challenge.movies.features.detail.view.cell.credits.CreditListAdapter
import com.exequiel.challenge.movies.features.detail.view.cell.video.VideoListAdapter
import com.exequiel.challenge.movies.features.detail.viewmodel.MovieDetailViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.Exception

class MovieDetailFragment: BaseFragment<MovieDetailFragmenBinding>() {

    private val detailViewModel: MovieDetailViewModel by viewModel()
    private val router: MovieDetailNavigation by inject()
    private val viewInputArgument: MovieDetailFragmentArgs by navArgs()

    override fun getViewModel() = detailViewModel

    override fun viewOnReady() {
        super.viewOnReady()
        initMoviesRecyclerView()
        initCastRecyclerView()
        initRemoteCalls()
    }

    private fun initRemoteCalls() {
        viewInputArgument.movieId.apply {
            detailViewModel.callDetailMovie(id = this)
            detailViewModel.callMoviesRelated(id = this)
            detailViewModel.callCredits(id = this)
        }
    }

    private fun initMoviesRecyclerView() {
        with(bindingView.videosRecyclerView) {
            this.adapter = VideoListAdapter(
                onItemClickListener = movieItemClickHandler,
            )
        }
    }

    private fun initCastRecyclerView() {
        with(bindingView.castRecyclerView) {
            this.adapter = CreditListAdapter(
                onItemClickListener = castItemClickHandler,
            )
        }
    }

    override fun bindObserversToLiveData() {
        super.bindObserversToLiveData()
        observe(detailViewModel.bindingDelegate.showImageBackground, this::showImageBackground)
        observe(detailViewModel.bindingDelegate.showTitle, this::showTitle)
        observe(detailViewModel.bindingDelegate.showGenres, this::showGenres)
        observe(detailViewModel.bindingDelegate.showRatingBar, this::showRatingBar)
        observe(detailViewModel.bindingDelegate.showVotes, this::showVotes)
        observe(detailViewModel.bindingDelegate.showDate, this::showDate)
        observe(detailViewModel.bindingDelegate.showTime, this::showTime)
        observe(detailViewModel.bindingDelegate.showLanguage, this::showLanguage)
        observe(detailViewModel.bindingDelegate.showDescription, this::showDescription)
        observe(detailViewModel.bindingDelegate.showVideosRelated, this::showVideosRelated)
        observe(detailViewModel.bindingDelegate.showCast, this::showCast)
    }

    private fun showImageBackground(event: Event<String>) {
        event.getContentIfNotHandled().let {
            if (it.isNullOrBlank()) {
                bindingView.movieImage.setImageResource(R.drawable.ic_baseline_image_24)
                return
            }else{
                bindingView.movieImageProgressBar.toVisible()
                Picasso.get().load(TheMovieDatabaseAPI.getBackdropUrl(it)).fit()
                    .transform(RoundedCornersTransformation(4, 1))
                    .error(R.drawable.ic_baseline_image_24)
                    .into(bindingView.movieImage, object : Callback {
                        override fun onSuccess() {
                            bindingView.movieImageProgressBar.toGone()
                        }

                        override fun onError(e: Exception?) {
                            bindingView.movieImageProgressBar.toGone()
                        }
                    })
            }
        }
    }

    private fun showTitle(event: Event<String>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.titleText.text = this
            }
        }
    }

    private fun showGenres(event: Event<String>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.genresText.text = this
            }
        }
    }

    private fun showRatingBar(event: Event<Float>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.ratingBar.rating = this
            }
        }
    }

    private fun showVotes(event: Event<String>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.numOfVotes.text = this
            }
        }
    }

    private fun showDate(event: Event<String>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.episodeText.text = this
            }
        }
    }

    private fun showTime(event: Event<String>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.seasonText.text = this
            }
        }
    }

    private fun showLanguage(event: Event<String>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.airDateText.text = this
            }
        }
    }

    private fun showDescription(event: Event<String>){
        event.getContentIfNotHandled().let {
            it?.apply {
                bindingView.overviewText.text = this
            }
        }
    }

    private fun showVideosRelated(event: Event<List<VideoModel>>){
        event.getContentIfNotHandled().let {
            it?.apply {
                (bindingView.videosRecyclerView.adapter as VideoListAdapter).submitList(this)
            }
        }
    }

    private fun showCast(event: Event<List<CastModel>>){
        event.getContentIfNotHandled().let {
            it?.apply {
                (bindingView.castRecyclerView.adapter as CreditListAdapter).submitList(this)
            }
        }
    }

    private val movieItemClickHandler = object : IOnItemClickViewHolder {
        override fun onItemClick(caller: View?, position: Int) {
            val key = (bindingView.videosRecyclerView.adapter as VideoListAdapter).currentList[position].key
            router.goToYoutubeVideo(requireActivity(), TheMovieDatabaseAPI.getYoutubeWatchUrl(key))
        }
    }

    private val castItemClickHandler = object : IOnItemClickViewHolder {
        override fun onItemClick(caller: View?, position: Int) {
            //Nothing for now
        }
    }
}


