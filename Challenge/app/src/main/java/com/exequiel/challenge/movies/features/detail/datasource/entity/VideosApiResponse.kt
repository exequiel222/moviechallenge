package com.exequiel.challenge.movies.features.detail.datasource.entity

import com.exequiel.challenge.movies.core.BaseListResponse
import com.google.gson.annotations.SerializedName

data class VideosApiResponse(
    @SerializedName("results")
    override var results: List<VideoApiResponse>
) : BaseListResponse<VideoApiResponse>

data class VideoApiResponse(
    @SerializedName("id")
    var id: String,

    @SerializedName("key")
    var key: String,

    @SerializedName("name")
    var name: String,
)