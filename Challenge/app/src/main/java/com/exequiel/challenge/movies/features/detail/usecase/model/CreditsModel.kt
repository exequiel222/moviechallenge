package com.exequiel.challenge.movies.features.detail.usecase.model

import com.exequiel.challenge.movies.core.BaseListResponse
import com.exequiel.challenge.movies.features.detail.datasource.entity.CastApiResponse
import com.google.gson.annotations.SerializedName

data class CreditsListModel(
    override var results: List<CastModel>
) : BaseListResponse<CastModel>

data class CastModel(
    var id: Int,
    var name: String,
    var profilePath: String?
)
