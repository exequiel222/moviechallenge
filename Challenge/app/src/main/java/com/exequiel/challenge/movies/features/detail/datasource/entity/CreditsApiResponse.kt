package com.exequiel.challenge.movies.features.detail.datasource.entity

import com.exequiel.challenge.movies.core.BaseListResponse
import com.google.gson.annotations.SerializedName

data class CreditsListApiResponse(
    @SerializedName("cast")
    override var results: List<CastApiResponse>
) : BaseListResponse<CastApiResponse>

data class CastApiResponse(
    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("profile_path")
    var profilePath: String?
)
