package com.exequiel.challenge.movies.features.list.view

import android.view.View
import com.exequiel.challenge.databinding.MovieListFragmentBinding
import com.exequiel.challenge.movies.core.InfiniteContentScrollListener
import com.exequiel.challenge.movies.core.view.BaseFragment
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.core.viewmodel.Event
import com.exequiel.challenge.movies.core.viewmodel.observe
import com.exequiel.challenge.movies.features.list.navigation.MoviesListNavigation
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel
import com.exequiel.challenge.movies.features.list.view.cell.MoviesListAdapter
import com.exequiel.challenge.movies.features.list.viewmodel.MoviesListViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoviesListFragment: BaseFragment<MovieListFragmentBinding>() {

    private val listViewModel: MoviesListViewModel by viewModel()
    private val router: MoviesListNavigation by inject()

    override fun getViewModel() = listViewModel

    override fun viewOnReady() {
        super.viewOnReady()
        initGridRecyclerView()
        listViewModel.loadMoreMovies()
    }

    override fun bindObserversToLiveData() {
        super.bindObserversToLiveData()
        observe(listViewModel.bindingDelegate.updateListItems, this::updateListItems)
    }

    private fun updateListItems(event: Event<List<MoviesItemModel>>){
        event.getContentIfNotHandled().let {
            it?.apply {
                (bindingView.moviesRecyclerView.adapter as MoviesListAdapter).submitList(this)
            }
        }
    }

    private fun initGridRecyclerView() {
        with(bindingView.moviesRecyclerView) {
            this.adapter = MoviesListAdapter(onItemClickListener = itemClickHandler,
                infiniteContentScrollListener = InfiniteContentScrollListener(this) {
                    listViewModel.loadMoreMovies()
                }
            )
        }
    }

    private val itemClickHandler = object : IOnItemClickViewHolder {
        override fun onItemClick(caller: View?, position: Int) {
            val id = (bindingView.moviesRecyclerView.adapter as MoviesListAdapter).currentList[position].id
            router.goToDetailMovie(requireView(), id)
        }
    }
}