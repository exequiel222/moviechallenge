package com.exequiel.challenge.movies.features.list.usecase.model

data class GenreModel(
    var id: Int,
    var name: String,
)