package com.exequiel.challenge.movies.features.list.datasource.service

import com.exequiel.challenge.movies.core.datasource.TheMovieDatabaseAPI.API_VERSION
import com.exequiel.challenge.movies.features.list.datasource.entity.MoviesItemApiResponse
import com.exequiel.challenge.movies.features.list.datasource.entity.MoviesApiResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IMoviesListService {

    @GET("/$API_VERSION/movie/popular")
    suspend fun fetchPopularList(@Query("page") page: Int): MoviesApiResponse

    @GET("/${API_VERSION}/movie/upcoming")
    suspend fun fetchUpcomingList(@Query("page") page: Int): MoviesApiResponse

    @GET("/${API_VERSION}/movie/now_playing")
    suspend fun fetchInTheatersList(@Query("page") page: Int): MoviesApiResponse

    @GET("/${API_VERSION}/discover/movie")
    suspend fun fetchDiscoverList(@Query("page") page: Int): MoviesApiResponse

    @GET("/${API_VERSION}/movie/{id}")
    suspend fun fetchDetails(@Path("id") id: Int): MoviesItemApiResponse
}