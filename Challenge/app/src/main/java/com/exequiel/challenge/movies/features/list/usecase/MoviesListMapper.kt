package com.exequiel.challenge.movies.features.list.usecase

import com.exequiel.challenge.movies.features.list.datasource.entity.GenreApiResponse
import com.exequiel.challenge.movies.features.list.datasource.entity.MoviesApiResponse
import com.exequiel.challenge.movies.features.list.datasource.entity.MoviesItemApiResponse
import com.exequiel.challenge.movies.features.list.usecase.model.GenreModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesModel

fun MoviesApiResponse.toUseCaseModel() = MoviesModel(
    page = page,
    results = results.map { it.toUseCaseModel() }
)

fun MoviesItemApiResponse.toUseCaseModel() = MoviesItemModel(
    id = id,
    posterPath = posterPath,
    backdropPath = backdropPath,
    title = title,
    voteCount = voteCount,
    voteAverage = voteAverage,
    genreIds = genreIds,
    originalLanguage = originalLanguage,
    releaseDate = releaseDate,
    runtime = runtime,
    overview = overview,
    genreResponses = genreResponses?.map { it.toUseCaseModel() }
)

fun GenreApiResponse.toUseCaseModel() = GenreModel(
    id = id,
    name = name
)