package com.exequiel.challenge.movies.features.detail.datasource.repository

import com.exequiel.challenge.movies.features.detail.datasource.service.MovieDetailService
import com.exequiel.challenge.movies.features.detail.usecase.model.CreditsListModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoListModel
import com.exequiel.challenge.movies.features.detail.usecase.toUseCaseModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesModel
import com.exequiel.challenge.movies.features.list.usecase.toUseCaseModel

class MovieDetailRepository(private val movieDetailService: MovieDetailService) {

    @Throws(Exception::class)
    suspend fun callGetDetailMovie(id: Int): MoviesItemModel {
        return movieDetailService.fetchDetails(id).toUseCaseModel()
    }

    @Throws(Exception::class)
    suspend fun callGetVideosMovie(id: Int): VideoListModel {
        return movieDetailService.fetchVideos(id).toUseCaseModel()
    }

    @Throws(Exception::class)
    suspend fun callGetCreditsMovie(id: Int): CreditsListModel {
        return movieDetailService.fetchCredits(id).toUseCaseModel()
    }
}