package com.exequiel.challenge.movies

import android.app.Application
import com.exequiel.challenge.movies.features.detail.module.movieDetailModule
import com.exequiel.challenge.movies.features.list.module.movieListModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class ChallengeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin(){
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@ChallengeApplication)
            modules(
                listOf(
                    movieListModule,
                    movieDetailModule
                )
            )
            koin.createRootScope()
        }
    }
}