package com.exequiel.challenge.movies.features.list.usecase

import com.exequiel.challenge.movies.core.usecase.BaseUseCase
import com.exequiel.challenge.movies.features.list.datasource.repository.MoviesListRepository
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesModel

class MoviesListUseCase(
    private val moviesListRepository: MoviesListRepository
) : BaseUseCase<Int, MoviesModel>() {

    override suspend fun run(params: Int): MoviesModel {
        return moviesListRepository.callGetPopularList(params)
    }
}