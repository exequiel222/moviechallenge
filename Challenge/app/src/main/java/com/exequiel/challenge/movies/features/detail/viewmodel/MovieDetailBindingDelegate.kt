package com.exequiel.challenge.movies.features.detail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.exequiel.challenge.movies.core.viewmodel.BaseBindingDelegate
import com.exequiel.challenge.movies.core.viewmodel.Event
import com.exequiel.challenge.movies.features.detail.usecase.model.CastModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesModel

class MovieDetailBindingDelegate : BaseBindingDelegate() {
    //region showImageBackground
    private val _showImageBackground = MutableLiveData<Event<String>>()
    val showImageBackground: LiveData<Event<String>> get() = _showImageBackground
    fun showImageBackgroundPostValue(urlImage: String) {
        _showImageBackground.value = Event(urlImage)
    }
    //endregion

    //region showTitle
    private val _showTitle = MutableLiveData<Event<String>>()
    val showTitle: LiveData<Event<String>> get() = _showTitle
    fun showTitlePostValue(text: String) {
        _showTitle.value = Event(text)
    }
    //endregion

    //region showGenres
    private val _showGenres = MutableLiveData<Event<String>>()
    val showGenres: LiveData<Event<String>> get() = _showGenres
    fun showGenresPostValue(text: String) {
        _showGenres.value = Event(text)
    }
    //endregion

    //region showRatingBar
    private val _showRatingBar = MutableLiveData<Event<Float>>()
    val showRatingBar: LiveData<Event<Float>> get() = _showRatingBar
    fun showRatingBarPostValue(rating: Float) {
        _showRatingBar.value = Event(rating)
    }
    //endregion

    //region showVotes
    private val _showVotes = MutableLiveData<Event<String>>()
    val showVotes: LiveData<Event<String>> get() = _showVotes
    fun showVotesPostValue(text: String) {
        _showVotes.value = Event(text)
    }
    //endregion

    //region showDate
    private val _showDate = MutableLiveData<Event<String>>()
    val showDate: LiveData<Event<String>> get() = _showDate
    fun showDatePostValue(text: String) {
        _showDate.value = Event(text)
    }
    //endregion

    //region showTime
    private val _showTime = MutableLiveData<Event<String>>()
    val showTime: LiveData<Event<String>> get() = _showTime
    fun showTimePostValue(text: String) {
        _showTime.value = Event(text)
    }
    //endregion

    //region showLanguage
    private val _showLanguage = MutableLiveData<Event<String>>()
    val showLanguage: LiveData<Event<String>> get() = _showLanguage
    fun showLanguagePostValue(text: String) {
        _showLanguage.value = Event(text)
    }
    //endregion

    //region showDescription
    private val _showDescription = MutableLiveData<Event<String>>()
    val showDescription: LiveData<Event<String>> get() = _showDescription
    fun showDescriptionPostValue(text: String) {
        _showDescription.value = Event(text)
    }
    //endregion

    //region showVideosRelated
    private val _showVideosRelated = MutableLiveData<Event<List<VideoModel>>>()
    val showVideosRelated: LiveData<Event<List<VideoModel>>> get() = _showVideosRelated
    fun showVideosRelatedPostValue(listMovies: List<VideoModel>) {
        _showVideosRelated.value = Event(listMovies)
    }
    //endregion

    //region showCast
    private val _showCast = MutableLiveData<Event<List<CastModel>>>()
    val showCast: LiveData<Event<List<CastModel>>> get() = _showCast
    fun showCastPostValue(listCastModel: List<CastModel>) {
        _showCast.value = Event(listCastModel)
    }
    //endregion
}