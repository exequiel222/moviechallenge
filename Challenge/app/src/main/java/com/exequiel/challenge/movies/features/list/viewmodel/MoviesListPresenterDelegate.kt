package com.exequiel.challenge.movies.features.list.viewmodel

import com.exequiel.challenge.movies.core.viewmodel.BasePresenterDelegate
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class MoviesListPresenterDelegate(
    private val bindingDelegate: MoviesListBindingDelegate
): BasePresenterDelegate(bindingDelegate) {

    fun initRemoteRequest(){
        bindingDelegate.showProgressViewPostValue()
    }

    fun onSuccessRequest(listItems: List<MoviesItemModel>){
        bindingDelegate.updateListItemsPostValue(listItems)
        bindingDelegate.hideProgressViewPostValue()
    }

    fun onErrorRequest(){
        bindingDelegate.hideProgressViewPostValue()
    }
}