package com.exequiel.challenge.movies.features.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.exequiel.challenge.movies.core.viewmodel.BaseBindingDelegate
import com.exequiel.challenge.movies.core.viewmodel.Event
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class MoviesListBindingDelegate : BaseBindingDelegate() {
    //region updateListItems
    private val _updateListItems = MutableLiveData<Event<List<MoviesItemModel>>>()
    val updateListItems: LiveData<Event<List<MoviesItemModel>>> get() = _updateListItems
    fun updateListItemsPostValue(listItems: List<MoviesItemModel>) {
        _updateListItems.value = Event(listItems)
    }
    //endregion
}