package com.exequiel.challenge.movies.features.detail.usecase

import com.exequiel.challenge.movies.features.detail.datasource.entity.CastApiResponse
import com.exequiel.challenge.movies.features.detail.datasource.entity.CreditsListApiResponse
import com.exequiel.challenge.movies.features.detail.datasource.entity.VideoApiResponse
import com.exequiel.challenge.movies.features.detail.datasource.entity.VideosApiResponse
import com.exequiel.challenge.movies.features.detail.usecase.model.CastModel
import com.exequiel.challenge.movies.features.detail.usecase.model.CreditsListModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoListModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoModel


fun VideosApiResponse.toUseCaseModel() = VideoListModel(
    results = results.map { it.toUseCaseModel() }
)

fun VideoApiResponse.toUseCaseModel() = VideoModel(
    id = id,
    key = key,
    name = name
)

fun CreditsListApiResponse.toUseCaseModel() = CreditsListModel(
    results = results.map { it.toUseCaseModel() }
)

fun CastApiResponse.toUseCaseModel() = CastModel(
    id = id,
    name = name,
    profilePath = profilePath
)