package com.exequiel.challenge.movies.features.detail.view.cell.credits

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.exequiel.challenge.databinding.MovieListItemCastBinding
import com.exequiel.challenge.databinding.MovieListItemVideoBinding
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.features.detail.usecase.model.CastModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoModel
import com.exequiel.challenge.movies.features.detail.view.cell.video.VideoListViewHolder
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class CreditListAdapter internal constructor(
    private val onItemClickListener: IOnItemClickViewHolder
) : ListAdapter<CastModel, CreditListViewHolder>(VideoDiffCallback()) {

    override fun onBindViewHolder(holder: CreditListViewHolder, position: Int) {
        holder.bindingDataInHolder(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreditListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = MovieListItemCastBinding.inflate(layoutInflater, parent, false)
        return CreditListViewHolder(binding, onItemClickListener)
    }

    private class VideoDiffCallback : DiffUtil.ItemCallback<CastModel>() {
        override fun areItemsTheSame(oldItem: CastModel, newItem: CastModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: CastModel, newItem: CastModel): Boolean {
            return oldItem.id == newItem.id
        }
    }
}