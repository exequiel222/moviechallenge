package com.exequiel.challenge.movies.features.list.viewmodel

import androidx.lifecycle.*
import com.exequiel.challenge.movies.core.viewmodel.BaseViewModel
import com.exequiel.challenge.movies.features.list.usecase.MoviesListUseCase
import androidx.lifecycle.viewModelScope
import com.exequiel.challenge.movies.core.appendList
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel
import kotlinx.coroutines.launch
import com.exequiel.challenge.movies.core.usecase.support.BaseResultWrapper

class MoviesListViewModel(
    override val bindingDelegate: MoviesListBindingDelegate,
    private val presenterDelegate: MoviesListPresenterDelegate = MoviesListPresenterDelegate(bindingDelegate),
    private val moviesListUseCase: MoviesListUseCase
): BaseViewModel(bindingDelegate,presenterDelegate) {

    private val movieList = MediatorLiveData<MutableList<MoviesItemModel>>()
    private var page: Int = 1

    fun loadMoreMovies() {
        callPopularMovies()
        page++
    }

    private fun callPopularMovies(){
        viewModelScope.launch{
            presenterDelegate.initRemoteRequest()
            when (val response = moviesListUseCase.invoke(page)) {
                is BaseResultWrapper.ApiError -> {
                    presenterDelegate.onErrorRequest()
                }
                is BaseResultWrapper.ApiSuccess -> {
                    movieList.appendList(response.value.results)
                    presenterDelegate.onSuccessRequest(movieList.value?.toList()!!)
                }
            }
        }
    }
}