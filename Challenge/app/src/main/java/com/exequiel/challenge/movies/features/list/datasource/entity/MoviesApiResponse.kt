package com.exequiel.challenge.movies.features.list.datasource.entity

import com.exequiel.challenge.movies.core.BasePageListResponse
import com.google.gson.annotations.SerializedName

data class MoviesApiResponse(
    @SerializedName("page")
    override var page: Int,

    @SerializedName("results")
    override var results: List<MoviesItemApiResponse>
) : BasePageListResponse<MoviesItemApiResponse>
