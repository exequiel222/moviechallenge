package com.exequiel.challenge.movies.features.detail.usecase.fetchmovies

import com.exequiel.challenge.movies.core.usecase.BaseUseCase
import com.exequiel.challenge.movies.features.detail.datasource.repository.MovieDetailRepository
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoListModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class FetchVideosRelatedUseCase(
    private val movieDetailRepository: MovieDetailRepository
) : BaseUseCase<Int, VideoListModel>() {

    override suspend fun run(params: Int): VideoListModel {
        return movieDetailRepository.callGetVideosMovie(params)
    }
}