package com.exequiel.challenge.movies.features.list.view

import android.os.Bundle
import com.exequiel.challenge.databinding.MovieListActivityBinding
import com.exequiel.challenge.movies.core.view.BaseActivity

class MoviesListActivity : BaseActivity<MovieListActivityBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}