package com.exequiel.challenge.movies.features.list.usecase.model

import com.exequiel.challenge.movies.core.BasePageListResponse

data class MoviesModel(
    override var page: Int,
    override var results: List<MoviesItemModel>
) : BasePageListResponse<MoviesItemModel>
