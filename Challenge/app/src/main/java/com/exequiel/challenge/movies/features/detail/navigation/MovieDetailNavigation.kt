package com.exequiel.challenge.movies.features.detail.navigation

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.FragmentActivity

class MovieDetailNavigation {
    fun goToYoutubeVideo(activity: FragmentActivity, uri: String){
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        activity.startActivity(intent)
    }
}