package com.exequiel.challenge.movies.core.viewmodel

import androidx.lifecycle.ViewModel
import com.exequiel.challenge.movies.core.viewmodel.BaseBindingDelegate
import com.exequiel.challenge.movies.core.viewmodel.BasePresenterDelegate

abstract class BaseViewModel(
    open val bindingDelegate: BaseBindingDelegate,
    private val presentationDelegate: BasePresenterDelegate
) : ViewModel() {

}