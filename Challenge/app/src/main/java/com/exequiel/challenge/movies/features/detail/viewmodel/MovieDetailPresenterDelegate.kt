package com.exequiel.challenge.movies.features.detail.viewmodel

import android.annotation.SuppressLint
import com.exequiel.challenge.movies.core.datasource.TheMovieDatabaseAPI
import com.exequiel.challenge.movies.core.viewmodel.BasePresenterDelegate
import com.exequiel.challenge.movies.features.detail.usecase.model.CastModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoListModel
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoModel
import com.exequiel.challenge.movies.features.list.usecase.model.GenreModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel
import java.text.SimpleDateFormat
import java.util.*

class MovieDetailPresenterDelegate(
    private val bindingDelegate: MovieDetailBindingDelegate
): BasePresenterDelegate(bindingDelegate) {

    fun initRemoteRequest(){
        bindingDelegate.showProgressViewPostValue()
    }

    fun onSuccessDetailMovieRequest(detailMovie: MoviesItemModel){
        with(bindingDelegate){
            detailMovie.let {
                showImageBackgroundPostValue(it.backdropPath ?: "")
                showTitlePostValue(it.title)
                showGenresPostValue(getGenresText(it.genreResponses))
                showRatingBarPostValue(getRatingBar(it.voteAverage, 5))
                showVotesPostValue( "${it.voteCount} votes ")
                showDatePostValue(getMovieRuntime(it.releaseDate))
                showTimePostValue(getMovieRuntime(it.runtime))
                showLanguagePostValue(getLanguageName(it.originalLanguage))
                showDescriptionPostValue(it.overview ?: "")
                hideProgressViewPostValue()
            }
        }
    }

    fun onSuccessVideosRelatedRequest(listMovies: List<VideoModel>){
        bindingDelegate.showVideosRelatedPostValue(listMovies)
    }

    fun onSuccessCastRequest(listCast: List<CastModel>){
        bindingDelegate.showCastPostValue(listCast)
    }

    fun onErrorRequest(){
        bindingDelegate.hideProgressViewPostValue()
    }

    private fun getGenresText(genres: List<GenreModel>?): String {
        if (genres == null) return ""

        val maxNumOfGenres = 3
        var text = ""
        val appendText = " / "

        val loopCount = if (genres.size <= maxNumOfGenres) genres.size else maxNumOfGenres
        for (i in 0 until loopCount) {
            text = text + genres[i].name + appendText
        }
        return text.dropLast(appendText.length)
    }

    private fun getRatingBar(voteAverage: Float, stars: Int): Float{
        return stars * ((voteAverage / TheMovieDatabaseAPI.MAX_RATING))
    }

    private fun getMovieRuntime(runtimeInMinutes: Int?): String{
        var result = ""
        runtimeInMinutes?.let {
            val hoursText: String = appendZeroBeforeNumber((it / 60f).toInt())
            val minutesText: String = appendZeroBeforeNumber((it % 60f).toInt())
            result = "$hoursText:$minutesText / $runtimeInMinutes min"
        }
        return result
    }

    private fun appendZeroBeforeNumber(num: Int): String {
        return if (num < 10) "0$num" else num.toString()
    }

    @SuppressLint("SimpleDateFormat")
    private fun getMovieRuntime(dateString: String?): String {
        var result = ""
        if (dateString.isNullOrBlank()) return result
        val date = SimpleDateFormat(TheMovieDatabaseAPI.getRuntimeDateFormat()).parse(dateString)
        val pat = SimpleDateFormat().toLocalizedPattern().replace("\\W?[HhKkmsSzZXa]+\\W?".toRegex(), "")
        val localFormatter = SimpleDateFormat(pat, Locale.getDefault())
        result = localFormatter.format(date)
        return result
    }

    private fun getLanguageName(languageCode: String?): String{
        var result = ""
        languageCode?.let { result = Locale(languageCode).getDisplayLanguage(Locale("en")) }
        return result
    }
}