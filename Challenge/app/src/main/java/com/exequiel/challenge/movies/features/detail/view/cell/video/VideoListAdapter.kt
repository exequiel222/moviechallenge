package com.exequiel.challenge.movies.features.detail.view.cell.video

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.exequiel.challenge.databinding.MovieListItemVideoBinding
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoModel
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class VideoListAdapter internal constructor(
    private val onItemClickListener: IOnItemClickViewHolder
) : ListAdapter<VideoModel, VideoListViewHolder>(VideoDiffCallback()) {

    override fun onBindViewHolder(holder: VideoListViewHolder, position: Int) {
        holder.bindingDataInHolder(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = MovieListItemVideoBinding.inflate(layoutInflater, parent, false)
        return VideoListViewHolder(binding, onItemClickListener)
    }

    private class VideoDiffCallback : DiffUtil.ItemCallback<VideoModel>() {
        override fun areItemsTheSame(oldItem: VideoModel, newItem: VideoModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: VideoModel, newItem: VideoModel): Boolean {
            return oldItem.id == newItem.id
        }
    }
}