package com.exequiel.challenge.movies.features.detail.view.cell.credits

import com.exequiel.challenge.R
import com.exequiel.challenge.databinding.MovieListItemCastBinding
import com.exequiel.challenge.movies.core.datasource.TheMovieDatabaseAPI
import com.exequiel.challenge.movies.core.view.cell.BaseViewHolder
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.features.detail.usecase.model.CastModel
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class CreditListViewHolder(
    private val binding: MovieListItemCastBinding,
    onItemClickListener: IOnItemClickViewHolder
) : BaseViewHolder<CastModel>(binding, onItemClickListener) {

    override fun bindingDataInHolder(data: CastModel) {
        super.bindingDataInHolder(data)
        bindPosterImageWithPicasso()
        bindName()
    }

    private fun bindName() {
        data?.apply {
            binding.titleText.text = data!!.name
        }
    }

    private fun bindPosterImageWithPicasso() {
        data?.apply {
            if (profilePath.isNullOrBlank()) {
                binding.image.setImageResource(R.drawable.ic_baseline_image_24)
                return
            }
            Picasso.get().load(TheMovieDatabaseAPI.getPosterUrl(profilePath!!)).fit()
                .transform(RoundedCornersTransformation(4, 1))
                .error(R.drawable.ic_baseline_image_24).into(binding.image)
        }
    }
}