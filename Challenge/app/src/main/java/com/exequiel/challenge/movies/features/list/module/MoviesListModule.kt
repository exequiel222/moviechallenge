package com.exequiel.challenge.movies.features.list.module

import com.exequiel.challenge.movies.core.datasource.ServiceBuilder
import com.exequiel.challenge.movies.features.list.datasource.repository.MoviesListRepository
import com.exequiel.challenge.movies.features.list.datasource.service.IMoviesListService
import com.exequiel.challenge.movies.features.list.navigation.MoviesListNavigation
import com.exequiel.challenge.movies.features.list.usecase.MoviesListUseCase
import com.exequiel.challenge.movies.features.list.viewmodel.MoviesListBindingDelegate
import com.exequiel.challenge.movies.features.list.viewmodel.MoviesListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val movieListModule: Module =  module {
    
    //Inject viewModel
    viewModel { MoviesListViewModel(bindingDelegate = get(), moviesListUseCase = get()) }
    factory { providerMoviesListBindingDelegate() }
    
    //Inject repository
    single {  MoviesListRepository(get()) }
    
    //Inject useCase
    single { MoviesListUseCase(MoviesListRepository(get())) }
    
    //Inject service
    single { providerMoviesListService() }

    //Routing component
    single { MoviesListNavigation() }

}

fun providerMoviesListBindingDelegate(): MoviesListBindingDelegate {
    return MoviesListBindingDelegate()
}

fun providerMoviesListService(): IMoviesListService {
    return ServiceBuilder.buildService(IMoviesListService::class.java)
}
        