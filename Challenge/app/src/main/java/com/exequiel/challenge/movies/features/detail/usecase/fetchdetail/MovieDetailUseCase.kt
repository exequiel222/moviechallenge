package com.exequiel.challenge.movies.features.detail.usecase.fetchdetail

import com.exequiel.challenge.movies.core.usecase.BaseUseCase
import com.exequiel.challenge.movies.features.detail.datasource.repository.MovieDetailRepository
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel

class MovieDetailUseCase(
    private val movieDetailRepository: MovieDetailRepository
) : BaseUseCase<Int, MoviesItemModel>() {

    override suspend fun run(params: Int): MoviesItemModel {
        return movieDetailRepository.callGetDetailMovie(params)
    }
}