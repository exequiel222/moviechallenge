package com.exequiel.challenge.movies.features.detail.datasource.service

import com.exequiel.challenge.movies.core.datasource.TheMovieDatabaseAPI.API_VERSION
import com.exequiel.challenge.movies.features.detail.datasource.entity.CreditsListApiResponse
import com.exequiel.challenge.movies.features.detail.datasource.entity.VideosApiResponse
import com.exequiel.challenge.movies.features.list.datasource.entity.MoviesApiResponse
import com.exequiel.challenge.movies.features.list.datasource.entity.MoviesItemApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieDetailService {

    @GET("/${API_VERSION}/movie/{id}")
    suspend fun fetchDetails(@Path("id") id: Int): MoviesItemApiResponse

    @GET("/$API_VERSION/movie/{id}/videos")
    suspend fun fetchVideos(@Path("id") id: Int): VideosApiResponse

    @GET("/$API_VERSION/movie/{id}/credits")
    suspend fun fetchCredits(@Path("id") id: Int): CreditsListApiResponse

}