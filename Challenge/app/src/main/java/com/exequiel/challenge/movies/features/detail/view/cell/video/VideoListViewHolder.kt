package com.exequiel.challenge.movies.features.detail.view.cell.video

import com.exequiel.challenge.R
import com.exequiel.challenge.databinding.MovieListItemVideoBinding
import com.exequiel.challenge.movies.core.datasource.TheMovieDatabaseAPI
import com.exequiel.challenge.movies.core.view.cell.BaseViewHolder
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.features.detail.usecase.model.VideoModel
import com.squareup.picasso.Picasso

class VideoListViewHolder(
    private val binding: MovieListItemVideoBinding,
    onItemClickListener: IOnItemClickViewHolder
) : BaseViewHolder<VideoModel>(binding, onItemClickListener) {

    override fun bindingDataInHolder(data: VideoModel) {
        super.bindingDataInHolder(data)
        bindVideoThumbnailWithPicasso(data.key)
    }

    private fun bindVideoThumbnailWithPicasso(youtubeId: String?) {
        if (youtubeId.isNullOrBlank()) {
            binding.image.setImageResource(R.drawable.ic_baseline_image_24)
            return
        }
        Picasso.get().load(TheMovieDatabaseAPI.getYoutubeImageUrl(youtubeId)).fit()
            .error(R.drawable.ic_baseline_image_24).into(binding.image)
    }
}