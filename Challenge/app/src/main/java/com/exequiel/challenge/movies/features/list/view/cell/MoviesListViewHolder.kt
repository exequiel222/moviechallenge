package com.exequiel.challenge.movies.features.list.view.cell

import android.widget.ImageView
import androidx.viewbinding.ViewBinding
import com.exequiel.challenge.R
import com.exequiel.challenge.databinding.MovieListItemGridBinding
import com.exequiel.challenge.databinding.MoviesListItemListBinding
import com.exequiel.challenge.movies.core.datasource.TheMovieDatabaseAPI
import com.exequiel.challenge.movies.core.view.cell.BaseViewHolder
import com.exequiel.challenge.movies.core.view.cell.IOnItemClickViewHolder
import com.exequiel.challenge.movies.features.list.usecase.model.MoviesItemModel
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class MoviesListViewHolder(
    private val binding: ViewBinding,
    onItemClickListener: IOnItemClickViewHolder
) : BaseViewHolder<MoviesItemModel>(binding, onItemClickListener) {

    override fun bindingDataInHolder(data: MoviesItemModel) {
        super.bindingDataInHolder(data)
        when (binding) {
            is MoviesListItemListBinding -> {
                bindPosterImage(binding)
            }
            is MovieListItemGridBinding -> {
                bindPosterImage(binding)
            }
            else -> throw Exception("Invalid list binding")
        }
    }

    private fun bindPosterImage(listBinding: MoviesListItemListBinding) {
        data?.apply {
            bindImage(this.posterPath, listBinding.image)
            listBinding.titleText.text = this.title
        }
    }

    private fun bindPosterImage(gridBinding: MovieListItemGridBinding) {
        data?.apply {
            bindImage(this.posterPath, gridBinding.image)
            gridBinding.titleText.text = this.title
        }
    }

    private fun bindImage(path: String?, imgView: ImageView){
        if (path.isNullOrBlank()) {
            imgView.setImageResource(R.drawable.ic_baseline_image_24)
            return
        }
        Picasso.get().load(TheMovieDatabaseAPI.getPosterUrl(path)).fit()
            .transform(RoundedCornersTransformation(4, 1))
            .error(R.drawable.ic_baseline_image_24).into(imgView)
    }
}