package com.exequiel.challenge.movies.features.detail.viewmodel

import androidx.lifecycle.viewModelScope
import com.exequiel.challenge.movies.core.viewmodel.BaseViewModel
import com.exequiel.challenge.movies.features.detail.usecase.fetchcast.FetchCastUseCase
import com.exequiel.challenge.movies.features.detail.usecase.fetchdetail.MovieDetailUseCase
import com.exequiel.challenge.movies.features.detail.usecase.fetchmovies.FetchVideosRelatedUseCase
import kotlinx.coroutines.launch
import com.exequiel.challenge.movies.core.usecase.support.BaseResultWrapper

class MovieDetailViewModel(
    override val bindingDelegate: MovieDetailBindingDelegate,
    private val presenterDelegate: MovieDetailPresenterDelegate = MovieDetailPresenterDelegate(bindingDelegate),
    private val movieDetailUseCase: MovieDetailUseCase,
    private val fetchVideosRelatedUseCase: FetchVideosRelatedUseCase,
    private val fetchCastUseCase: FetchCastUseCase
): BaseViewModel(bindingDelegate,presenterDelegate) {

    fun callDetailMovie(id: Int){
        viewModelScope.launch{
            presenterDelegate.initRemoteRequest()
            when (val response = movieDetailUseCase.invoke(id)) {
                is BaseResultWrapper.ApiError -> {
                    presenterDelegate.onErrorRequest()
                }
                is BaseResultWrapper.ApiSuccess -> {
                    presenterDelegate.onSuccessDetailMovieRequest(response.value)
                }
            }
        }
    }

    fun callMoviesRelated(id: Int){
        viewModelScope.launch{
            when (val response = fetchVideosRelatedUseCase.invoke(id)) {
                is BaseResultWrapper.ApiError -> {
                    presenterDelegate.onErrorRequest()
                }
                is BaseResultWrapper.ApiSuccess -> {
                    presenterDelegate.onSuccessVideosRelatedRequest(response.value.results)
                }
            }
        }
    }

    fun callCredits(id: Int){
        viewModelScope.launch{
            when (val response = fetchCastUseCase.invoke(id)) {
                is BaseResultWrapper.ApiError -> {
                    presenterDelegate.onErrorRequest()
                }
                is BaseResultWrapper.ApiSuccess -> {
                    presenterDelegate.onSuccessCastRequest(response.value.results)
                }
            }
        }
    }
}