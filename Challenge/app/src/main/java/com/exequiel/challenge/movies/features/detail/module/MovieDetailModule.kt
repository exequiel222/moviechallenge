package com.exequiel.challenge.movies.features.detail.module

import com.exequiel.challenge.movies.core.datasource.ServiceBuilder
import com.exequiel.challenge.movies.features.detail.datasource.repository.MovieDetailRepository
import com.exequiel.challenge.movies.features.detail.datasource.service.MovieDetailService
import com.exequiel.challenge.movies.features.detail.navigation.MovieDetailNavigation
import com.exequiel.challenge.movies.features.detail.usecase.fetchcast.FetchCastUseCase
import com.exequiel.challenge.movies.features.detail.usecase.fetchdetail.MovieDetailUseCase
import com.exequiel.challenge.movies.features.detail.usecase.fetchmovies.FetchVideosRelatedUseCase
import com.exequiel.challenge.movies.features.detail.viewmodel.MovieDetailBindingDelegate
import com.exequiel.challenge.movies.features.detail.viewmodel.MovieDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val movieDetailModule: Module =  module {
    
    //Inject viewModel
    viewModel { MovieDetailViewModel(
        bindingDelegate = get(),
        movieDetailUseCase = get(),
        fetchVideosRelatedUseCase = get(),
        fetchCastUseCase = get()
    ) }
    factory { providerMoviesListBindingDelegate() }
    
    //Inject repository
    single {  MovieDetailRepository(get()) }
    
    //Inject useCase
    single { MovieDetailUseCase(get()) }
    single { FetchVideosRelatedUseCase(get()) }
    single { FetchCastUseCase(get()) }
    
    //Inject service
    single { providerMoviesListService() }

    //Routing component
    single { MovieDetailNavigation() }

}

fun providerMoviesListBindingDelegate(): MovieDetailBindingDelegate {
    return MovieDetailBindingDelegate()
}

fun providerMoviesListService(): MovieDetailService {
    return ServiceBuilder.buildService(MovieDetailService::class.java)
}
        